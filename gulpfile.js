var gulp = require('gulp');
var less = require('gulp-less');
var imagemin = require('gulp-imagemin');
var browserSync = require('browser-sync').create();

gulp.task('less', function(){
  gulp.src('./assets/stylesheets/less_style/main.less')
    .pipe(less())
    .pipe(gulp.dest('./assets/stylesheets/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: './'
    },
    browser: "chrome.exe",
    // browser: "Google Chrome",
  })
});

gulp.task('imagemin', () =>
    gulp.src('./assets/img/mini/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./assets/img'))
);

gulp.task('watch', ['browserSync', 'less'], function (){
  gulp.watch('./assets/stylesheets/less_style/**/*.less', ['less']);
  gulp.watch('./*.html', browserSync.reload);
  gulp.watch('assets/js/**/*.js', browserSync.reload);
});
