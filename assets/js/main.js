'use strict';
!function(){

  $('#main-nav li').click(function(e) {
    e.preventDefault();
    $("#main-nav li").removeClass("active");
    $(this).addClass('active');
  });

  $('.hamburger-container').click(function() {
    $('.line-1, .line-2, .line-3').toggleClass('is-clicked');
    $('.menu-overlay').toggleClass('is-active');
  });

  $('.closeBtn').click(function() {
    $('.menu-overlay').removeClass('is-active');
    $('.line-1, .line-2, .line-3').removeClass('is-clicked');
  });

  var events = $('.big-event').length;
  var max = 0;
  
  $('.big-event').each(function(i,obj){
    max = $(this).height() > max ? $(this).height() : max;
  });
  
  $('.big-event').css('height', max);

}();
